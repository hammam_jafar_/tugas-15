@extends('layout.master')

@section('judul')
Daftar Sekarang
@endsection

@section('content')
<form action="/submit" method="Post">
    @csrf
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
        First Name:
        <br>
        <br>
    <input type="text" name="Nama Depan"> <br><br>
        Last Name:
        <br>
        <br>
    <input type="text" name="Nama Belakang"><br><br>
    Gender : <br><br>
    <input type="Radio" name="Gender" value="Male"> Male <br>
    <input type="Radio" name="Gender" value="Female"> Female <br>
    <input type="Radio" name="Gender" value="Other"> Other <br><br>
        Nationality: <br>
        <br> <select name="Nationality">
            <option value="Indonesian">Indonesian</option>
            <option value="Amerika">Amerika</option>
            <option value="Inggris">Inggris</option>
            <option value="Asing">Asing</option> <br> <br>
        </select><br>
    <br> Language Spoken <br><br>
    <input type="checkbox" name="Bahasa Indonesia"> Bahasa Indonesia <br>
    <input type="checkbox" name="English"> English <br>
    <input type="checkbox" name="Other"> Other <br>
    <br>Bio: <br><br>
    <textarea name="Biodata" id="" cols="30" rows="10"></textarea><br><br>
    <br><input type="Submit" value="Sign Up">
</form>
@endsection