@extends('layout.master')

@section('judul')
Biodata
@endsection

@section('content')


<form action='/cast' method='post'>
    @csrf
    <div class="form-group">
      <label> Nama Lengkap</label>
      <input type="text" name="nama" class="form-control" placeholder="Muhammad Hammam Jafar">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label> Umur</label>
        <input type="text" name="umur" class="form-control" placeholder="20">
      </div>
      @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label for="exampleFormControlTextarea1">Biodata</label>
      <textarea name="bio" class="form-control" rows="3"></textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  <button type="submit" class="btn btn-primary mb-2">Kirim</button>
  </form>


@endsection