@extends('layout.master')

@section('judul')
Edit bio {{$cast->nama}}
@endsection

@section('content')


<form action='/cast/{{$cast->id}}' method='POST'>
    @csrf
    @method('put')
    <div class="form-group">
      <label> Nama Lengkap</label>
      <input type="text" name="nama" value="{{$cast->nama}}" class="form-control" placeholder="Muhammad Hammam Jafar">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label> Umur</label>
        <input type="text" name="umur"  value="{{$cast->umur}}" class="form-control" placeholder="20">
      </div>
      @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label for="exampleFormControlTextarea1">Biodata</label>
      <textarea name="bio"  class="form-control" cols="15" rows="15">{{$cast->bio}}</textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  <button type="submit" class="btn btn-primary mb-2">Update</button>
  </form>


@endsection