<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', "IndexController@index" );
Route::get('/isidata', "AuthController@index" );
Route::post('/submit', "AuthController@submit" );

Route::get('/master', function(){
    return view('layout.master');
});
Route::get('/table', function(){
    return view('table.table');
});
Route::get('/data-table', function(){
    return view('table.data-table');
});

//CRUD Cast

Route::get('/cast/create','castcontroller@create');
Route::post('/cast','castcontroller@store');
Route::get('/cast','castcontroller@index');
Route::get('/cast/{cast_id}','castcontroller@show');
Route::get('/cast/{cast_id}/edit','castcontroller@edit');
Route::put('/cast/{cast_id}','castcontroller@update');
Route::delete('/cast/{cast_id}','castcontroller@destroy');

